<html>

<head>
  <base href="/" />
  <title>Ceci est mon titre</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
  <script>
    function checkImage(message) {
      fetch(message)
        .then(result => {
          console.log("c'est une bonne image");
          document.location.href = "/index.php?url=" + message;
        })
        .catch(() => {
          console.error("l'image est corrompue");
        });
    }

    function getImage() {
      if (window.fetch) {
        // file or JSON document
        fetch("https://dog.ceo/api/breeds/image/random")
          .then(function(response) {
            return response.json();
          })
          .catch(function(err) {
            console.error(err);
          })
          .then(function(data) {
            console.log(data);
            if (data.status === 'success') {
              // checkImage(data.message);
              document.location.href = "/index.php?url=" + data.message;
            } else {
              console.error('pas de message cette fois');
            }
          });
      } else {
        console.log("fetch is not supported by your browser");
      }
    }

    async function getImageNext() {
      try {
        const response = await fetch("https://dog.ceo/api/breeds/image/random");
        const data = await response.json();
        document.location.href = "/index.php?url=" + data.message;
      } catch (err) {
        console.error(err);
      }
    }
  </script>

  <p>
    <?php
    if (isset($_GET['url'])) {
      $url = $_GET['url'];
      echo '<p><button onclick="getImageNext()">Reload image</button></p>';
      echo "<img src='" . $url . "'>";
    } else {
    ?>
      <script>
        getImage();
      </script>
    <?php
      echo "<h3>Loading...</h3>";
    }
    ?>
  </p>
</body>

</html>